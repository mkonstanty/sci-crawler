class HomeController < ApplicationController
  
  before_action :authenticate_user!
  
  def index
  end
  
  def resource
    @user || User.new
  end
  
  helper_method :resource
end